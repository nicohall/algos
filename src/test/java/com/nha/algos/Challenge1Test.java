package com.nha.algos;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class Challenge1Test {

    @Test
    void nominalAlpha() {
        assertThat(new Challenge1().transform("a")).isEqualTo("b");
    }

    @Test
    void nominalVowel() {
        assertThat(new Challenge1().transform("d")).isEqualTo("E");
    }

    @Test
    void nominalNonAlpha() {
        assertThat(new Challenge1().transform("-")).isEqualTo("-");
    }

    @Test
    void nominalLongInput() {
        assertThat(new Challenge1().transform("4PTC3,<Bwu8GY@D7\n" +
                "bKç\\^2QvxçufKkf?\n" +
                "C-Q!s4;r^}>ékpc\\\n" +
                "i¤X[?Mi-fVàcP!e/\n" +
                "i:dl'hq)@?>HSàr;\n" +
                "uq06N?q§µpSkbA6$\n" +
                "Jk`^Nà8y/e{¨j9>@\n" +
                "DZ=TZO4TdoPMERIj\n" +
                "%?t8P¤n;{@r]JXZ1\n" +
                "2\"_£{J\\¤G¤!~BKµ\"")).isEqualTo("4QUD3,<Cxv8HZ@E7\n" +
                "cLç\\^2RwYçvgLlg?\n" +
                "D-R!t4;s^}>élqd\\\n" +
                "j¤Y[?Nj-gWàdQ!f/\n" +
                "j:Em'Ir)@?>ITàs;\n" +
                "vr06O?r§µqTlcB6$\n" +
                "Kl`^Oà8z/f{¨k9>@\n" +
                "EA=UAP4UEpQNFSJk\n" +
                "%?U8Q¤O;{@s]KYA1\n" +
                "2\"_£{K\\¤H¤!~CLµ\"");
    }

}