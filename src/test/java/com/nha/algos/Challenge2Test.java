package com.nha.algos;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class Challenge2Test {

    @Test
    void nominalHitMaxFirst() {
        assertThat(new Challenge2().transform(new int[]{6, 1, 2, 3})).isEqualToIgnoringCase(Boolean.TRUE.toString());
    }

    @Test
    void nominalHitMaxAnyWhere() {
        assertThat(new Challenge2().transform(new int[]{1, 2, 6, 3})).isEqualToIgnoringCase(Boolean.TRUE.toString());
    }

    @Test
    void nominalHitMaxLast() {
        assertThat(new Challenge2().transform(new int[]{1, 2, 3, 6})).isEqualToIgnoringCase(Boolean.TRUE.toString());
    }

    @Test
    void nominalMissMaxFirst() {
        assertThat(new Challenge2().transform(new int[]{7, 1, 2, 3})).isEqualToIgnoringCase(Boolean.FALSE.toString());
    }

    @Test
    void nominalMissMaxAnyWhere() {
        assertThat(new Challenge2().transform(new int[]{1, 2, 7, 3})).isEqualToIgnoringCase(Boolean.FALSE.toString());
    }

    @Test
    void nominalMissMaxLast() {
        assertThat(new Challenge2().transform(new int[]{1, 2, 3, 7})).isEqualToIgnoringCase(Boolean.FALSE.toString());
    }

}