package com.nha.algos;

/**
 * Input is an array of int
 * the sum of all the elements of the array except the maximum should be compared to
 * the maximum element of the array.
 * If it's equals, return "true", if it's not, return false.
 * Ex: {1, 2, 3, 4} -> "false"
 * Ex 2: {1, 2, 3, 6} -> "true"
 */
public class Challenge2 {

    public String transform(int[] input) {
        throw new IllegalStateException("Implement me");
    }
}
