package com.nha.algos;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This challenge aim to transform a string with the following rules:
 * - the string in input can contain any character but is neither null nor empty
 * - any alphabetical character should be replaced by the following char in ascii (a->b, b->c, ... z->a)
 * - any vowel should be upper cased
 */
public class Challenge1 {

    public String transform(String input) {

        throw new IllegalStateException("Implement me");
    }
}
